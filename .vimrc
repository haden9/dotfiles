set nocompatible              " be iMproved, required
filetype off                  " required
let gitlab = 'https://gitlab.com/' " Adding support for non-github projects

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Other Plugins
Plugin 'kien/ctrlp.vim'
Plugin 'Raimondi/delimitMate'
Plugin 'Yggdroot/indentLine'
Plugin 'vim-ruby/vim-ruby'
Plugin 'mileszs/ack.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'mg979/vim-visual-multi'
Plugin 'Valloric/YouCompleteMe', { 'do': './install.py --all' }
Plugin 'hashivim/vim-terraform'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'dense-analysis/ale'
Plugin 'nvie/vim-flake8'
Plugin 'iamcco/markdown-preview.nvim' "Don't forget to :call mkdp#util#install()
Plugin gitlab.'haden9/vim-spotifysearch'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal

" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Vim options
syntax on
set nocompatible
set number
set tabstop=2
set shiftwidth=2
set expandtab
retab
set list
set noswapfile
highlight ColorColumn ctermbg=darkgrey
let &colorcolumn="80,".join(range(100,999),",")

" Vim file/dir options
set listchars=tab:>-,trail:~,extends:>,precedes:<
autocmd FileType ruby,eruby,yaml,python,terraform autocmd BufWritePre <buffer> :%s/\s\+$//e
autocmd FileType ruby map <F9> :w<CR>:!ruby -c %<CR>
autocmd BufEnter * silent! lcd %:p:h
" End Vim options & file/dir options

" indentLine
let g:indentLine_color_term = 239
" End indentLine

" vim-ruby
let g:rubycomplete_buffer_loading = 1
let g:rubycomplete_rails = 1
" End vim-ruby

" ctrlp-vim
set runtimepath^=~/.vim/bundle/ctrlp.vim
set wildignore+=*/coverage/*,*/log/*,*/tmp/*,*.so,*.swp,*.zip,*/node_modules/*,*/dist/*
let g:ctrlp_show_hidden = 0
" End ctrlp-vim

" silver_searcher
let g:ag_prg="/usr/local/bin/ag -H --nocolor --nogroup --ignore-dir=log --ignore-dir=tmp --column --vimgrep"
" End silver_searcher

" You Complete Me
let g:ycm_min_num_of_chars_for_completion = 4
let g:ycm_min_num_identifier_candidate_chars = 4
let g:ycm_enable_diagnostic_highlighting = 0
set completeopt-=preview
let g:ycm_add_preview_to_completeopt = 0
" End You Complete Me

" ALE
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'javascript': ['eslint'],
\}
let g:ale_fixers.python = ['black']
let g:ale_fix_on_save = 1
let g:ale_completion_enabled = 0
" End ALE

" vim-terraform
let g:terraform_fmt_on_save = 1
let g:terraform_align = 1
" End vim-terraform

" ack.vim
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif
" End ack.vim

" MarkdownPreview
let g:mkdp_auto_start = 0
let g:mkdp_auto_close = 1
let g:mkdp_refresh_slow = 0
let g:mkdp_command_for_global = 0
let g:mkdp_open_to_the_world = 0
let g:mkdp_open_ip = ''
let g:mkdp_browser = ''
let g:mkdp_echo_preview_url = 0
let g:mkdp_browserfunc = ''
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 1,
    \ 'sequence_diagrams': {},
    \ 'flowchart_diagrams': {},
    \ 'content_editable': v:false,
    \ 'disable_filename': 0,
    \ 'toc': {}
    \ }
let g:mkdp_markdown_css = ''
let g:mkdp_highlight_css = ''
let g:mkdp_port = ''
let g:mkdp_page_title = '「${name}」'
let g:mkdp_filetypes = ['markdown']
let g:mkdp_theme = 'dark'
" End MarkdownPreview
